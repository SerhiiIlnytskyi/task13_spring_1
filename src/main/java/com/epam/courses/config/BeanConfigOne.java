package com.epam.courses.config;

import com.epam.courses.bean.BeanA;
import com.epam.courses.bean.BeanB;
import com.epam.courses.bean.BeanC;
import com.epam.courses.bean.BeanD;
import com.epam.courses.bean.BeanE;
import com.epam.courses.bean.BeanBChangeInitMethodBeanFactoryPostProcessor;
import com.epam.courses.bean.FieldValidatorBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({BeanConfigTwo.class})
public class BeanConfigOne {


  @Bean(value = "changeMethods", initMethod = "init")
  public BeanBChangeInitMethodBeanFactoryPostProcessor changeMethods() {
    return new BeanBChangeInitMethodBeanFactoryPostProcessor();
  }

  @Bean
  public FieldValidatorBeanPostProcessor validateFields() {
    return new FieldValidatorBeanPostProcessor();
  }

  @Bean("firstBeanA")
  public BeanA getFirstBeanA(BeanB beanB, BeanC beanC) {
    return new BeanA(null, beanC.getValue());
  }

  @Bean("secondBeanA")
  public BeanA getSecondBeanA(BeanB beanB, BeanD beanD) {
    return new BeanA(beanB.getName(), beanD.getValue());
  }

  @Bean("thirdBeanA")
  public BeanA getThirdBeanA(BeanC beanC, BeanD beanD) {
    return new BeanA(beanC.getName(), beanD.getValue());
  }

  @Bean("FirstBeanE")
  public BeanE getFirstBeanE(BeanA firstBeanA) {
    return new BeanE(firstBeanA.getName(), firstBeanA.getValue());
  }

  @Bean("SecondBeanE")
  public BeanE getSecondBeanE(BeanA secondBeanA) {
    return new BeanE(secondBeanA.getName(), secondBeanA.getValue());
  }

  @Bean("ThirdBeanE")
  public BeanE getThirdBeanE(BeanA thirdBeanA) {
    return new BeanE(thirdBeanA.getName(), thirdBeanA.getValue());
  }

}
