package com.epam.courses.config;

import com.epam.courses.bean.BeanB;
import com.epam.courses.bean.BeanC;
import com.epam.courses.bean.BeanD;
import com.epam.courses.bean.BeanF;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("task.properties")
public class BeanConfigTwo {

  @Value("${beanB.name}")
  private String nameB;
  @Value("${beanB.value}")
  private Integer valueB;

  @Value("${beanC.name}")
  private String nameC;
  @Value("${beanC.value}")
  private Integer valueC;

  @Value("${beanD.name}")
  private String nameD;
  @Value("${beanD.value}")
  private Integer valueD;

  @Bean(value = "beanB", initMethod = "init", destroyMethod = "destroy")
  @DependsOn(value = "beanD")
  public BeanB getBeanB() {
    return new BeanB(nameB, valueB);
  }

  @Bean(value = "beanC", initMethod = "init", destroyMethod = "destroy")
  @DependsOn(value = "beanB")
  public BeanC getBeanC() {
    return new BeanC(nameC, valueC);
  }

  @Bean(value = "beanD", initMethod = "init", destroyMethod = "destroy")
  public BeanD getBeanD() {
    return new BeanD(nameD, valueD);
  }

  @Bean("beanF")
  @Lazy
  public BeanF getBeanF() {
    return new BeanF();
  }
}
