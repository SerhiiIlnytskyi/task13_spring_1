package com.epam.courses;

import com.epam.courses.config.BeanConfigOne;
import java.util.Arrays;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

  public static void main(String[] args) {
    ApplicationContext context = new AnnotationConfigApplicationContext(
        BeanConfigOne.class);
    System.out.printf("\nAll Beans From Context: \n");
    Arrays.stream(context.getBeanDefinitionNames())
        .forEach(System.out::println);
    System.out.println();
    ((AnnotationConfigApplicationContext) context).close();
  }
}
