package com.epam.courses.bean;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanC implements BeanValidator {

  private static final Logger logger = LogManager.getLogger(BeanC.class);

  private String name;

  private Integer value;

  public BeanC(String nameC, Integer valueC) {
    this.name = nameC;
    this.value = valueC;
    System.out.println("BeanC Constructor " + this);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getValue() {
    return value;
  }

  public void setValue(Integer value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanC{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }

  @Override
  public void validate() {
    if (name == null) {
      logger.log(Level.ERROR, "Name can't be null BeanC");
    }
    if (value < 0) {
      logger.log(Level.ERROR, "Value must be greater then zero BeanC");
    }
    System.out.println("BeanC validate method (BeanValidator) " + this);
  }

  private void init() {
    System.out.println("BeanC init method " + this);
  }

  private void destroy() {
    System.out.println("BeanC destroy method " + this);
  }
}
