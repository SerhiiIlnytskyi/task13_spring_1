package com.epam.courses.bean;

@FunctionalInterface
public interface BeanValidator {

  void validate();
}
