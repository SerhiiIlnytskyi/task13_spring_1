package com.epam.courses.bean;


import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

public class BeanBChangeInitMethodBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

  @Override
  public void postProcessBeanFactory(
      ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
    BeanDefinition beanBDefinition = configurableListableBeanFactory.getBeanDefinition("beanB");
    if (beanBDefinition != null) {
      beanBDefinition.setInitMethodName("secondInit");
    }

    String[] names = configurableListableBeanFactory.getBeanDefinitionNames();
    for (String name: names) {
      System.out.println(configurableListableBeanFactory.getBeanDefinition(name));
    }

  }

  private void init() {
    System.out.println("BeanBChangeInitMethodBeanFactoryPostProcessor init " + this);
  }
}
