package com.epam.courses.bean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanE implements BeanValidator {

  private static final Logger logger = LogManager.getLogger(BeanE.class);

  private String name;
  private Integer value;

  public BeanE(String name, Integer value) {
    this.name = name;
    this.value = value;
    System.out.println("BeanE Constructor " + this);
  }

  @PostConstruct
  public void postConstructMethod() {
    System.out.println("BeanE @PostConstruct " + this);
  }

  @PreDestroy
  public void preDestroyMethod() {
    System.out.println("BeanE @PreDestroy " + this);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getValue() {
    return value;
  }

  public void setValue(Integer value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanE{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }


  @Override
  public void validate() {
    if (name == null) {
      logger.log(Level.ERROR, "Name can't be null BeanE");
    }
    if (value < 0) {
      logger.log(Level.ERROR, "Value must be greater then zero BeanE");
    }
    System.out.println("BeanE validate method (BeanValidator) " + this);
  }
}
