package com.epam.courses.bean;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanB implements BeanValidator {

  private static final Logger logger = LogManager.getLogger(BeanB.class);

  private String name;

  private Integer value;

  public BeanB(String name, Integer value) {
    this.name = name;
    this.value = value;
    System.out.println("BeanB Constructor " + this);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getValue() {
    return value;
  }

  public void setValue(Integer value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanB{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }

  @Override
  public void validate() {
    if (name == null) {
      logger.log(Level.ERROR, "Name can't be null BeanB");
    }
    if (value < 0) {
      logger.log(Level.ERROR, "Value must be greater then zero BeanB");
    }
    System.out.println("BeanB validate method (BeanValidator) " + this);
  }

  private void init() {
    System.out.println("BeanB init method " + this);
  }

  private void destroy() {
    System.out.println("BeanB destroy method " + this);
  }

  private void secondInit() {
    System.out.println("BeanB second init method " + this);
  }
}
