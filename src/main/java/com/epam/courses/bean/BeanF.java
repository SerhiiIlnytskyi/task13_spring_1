package com.epam.courses.bean;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanF implements BeanValidator {

  private static final Logger logger = LogManager.getLogger(BeanF.class);

  private String name;
  private Integer value;

  public BeanF() {
    System.out.println("BeanF Constructor " + this);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getValue() {
    return value;
  }

  public void setValue(Integer value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanF{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }

  @Override
  public void validate() {
    if (name == null) {
      logger.log(Level.ERROR, "Name can't be null BeanF");
    }
    if (value < 0) {
      logger.log(Level.ERROR, "Value must be greater then zero BeanF");
    }
    System.out.println("BeanF validate method (BeanValidator) " + this);
  }
}
