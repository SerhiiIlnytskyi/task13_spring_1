package com.epam.courses.bean;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {

  private static final Logger logger = LogManager.getLogger(BeanA.class);

  private String name;
  private Integer value;

  public BeanA() {
    System.out.println("BeanA Constructor " + this);
  }

  public BeanA(String name, Integer value) {
    this.name = name;
    this.value = value;
    System.out.println("BeanA Constructor " + this);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getValue() {
    return value;
  }

  public void setValue(Integer value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanA{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }



  @Override
  public void validate() {
    if (name == null) {
      logger.log(Level.ERROR, "Name can't be null BeanA");
    }
    if (value < 0) {
      logger.log(Level.ERROR, "Value must be greater then zero BeanA");
    }
    System.out.println("BeanA validate method (BeanValidator) " + this);
  }

  @Override
  public void destroy() throws Exception {
    System.out.println("BeanA destroy method (DisposableBean) " + this);
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    System.out.println("BeanA afterPropertiesSet method (InitializingBean) " + this);
  }
}
