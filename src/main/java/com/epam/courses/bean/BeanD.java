package com.epam.courses.bean;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanD implements BeanValidator {

  private static final Logger logger = LogManager.getLogger(BeanD.class);

  private String name;

  private Integer value;

  public BeanD(String nameD, Integer valueD) {
    this.name = nameD;
    this.value = valueD;
    System.out.println("BeanD Constructor " + this);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getValue() {
    return value;
  }

  public void setValue(Integer value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanD{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }

  @Override
  public void validate() {
    if (name == null) {
      logger.log(Level.ERROR, "Name can't be null BeanD");
    }
    if (value < 0) {
      logger.log(Level.ERROR, "Value must be greater then zero BeanD");
    }
    System.out.println("BeanD validate method (BeanValidator) " + this);
  }

  private void init() {
    System.out.println("BeanD init method " + this);
  }

  private void destroy() {
    System.out.println("BeanD destroy method " + this);
  }

}
